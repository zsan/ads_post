
/**
 * Module dependencies.
 */

require('coffee-script');
var express = require('express')
  , http = require('http')
  , path = require('path')
  , passport = require('passport')
  , mongoose = require('mongoose');  

require('express-namespace');

require('./config/db-connection')(mongoose)

// required models
var Ad   = require('./apps/models/ad')(mongoose);
var User = require('./apps/models/user')(mongoose);

require('./config/passport')(passport, User)

var app    = express();
var server = http.createServer(app);

// all environments
app.set('port', process.env.PORT || 3000);

// useless ?
app.set('views', __dirname + '/views');

app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session({secret: 'metallica pantera'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(require('connect-assets')());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


//require environment's config
require('./config/environment')(app, express)

//Routes
require('./apps/controllers/authentication_routes')(app,passport)
require('./apps/controllers/public_routes')(app, Ad)
require('./apps/controllers/admin_routes')(app, Ad, passport)
require('./apps/controllers/ads_routes')(app, Ad)

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

require('./apps/socket-io')(app, server);
