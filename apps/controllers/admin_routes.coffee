
routes = (app, ad_model) ->
  app.namespace '/admin', ->
    app.all '/*', (req, res, next) ->
      if req.isAuthenticated()
        #res.redirect '/admin/ads'
        #return
        next()
        #return
      else
        req.session_redirect_to = req.path
        res.redirect '/login'
        return
      

    app.namespace '/ads', ->
      app.get '/',(req,res) ->
        ad = new ad_model {}
        
        ad_model.find().sort
          date: -1
        .exec((err, ads) ->
          res.render "#{__dirname}/../views/admin/all",
            title: "Views All Ads"
            stylesheet: 'admin'
            ad: ad
            ads: ads)

      app.get '/destroy/:id',(req,res) ->
        ad_model.remove 
          _id: req.params.id, (err, msg) ->
            if err
              console.log(err)

            if socketIO = app.settings.socketIO
              socketIO.sockets.emit "ad:remove", msg
            res.redirect '/admin/ads'    
      

module.exports = routes
