routes = (app, ad_model) ->       

  app.namespace '/ads', ->
    app.get '/new', (req, res) ->
      res.render "#{__dirname}/../views/ads/_form",
        title: "Iklan Gratis"
        stylesheet: 'sidewalk'
        message: req.session.messages

    app.get '/:id', (req, res, next) ->
      ad_model.findById(req.params.id).exec (err, ad) ->
        if err or not ad
          return res.redirect '/'

      

        res.render "#{__dirname}/../views/ads/show",
          title: "Jual #{ad.name}"
          stylesheet: 'sidewalk'
          ad: ad


    app.post '/new', (req,res) ->
      ttl = req.body.expired
      
      now = new Date
      # (60 * 60000) = 1 hour
      # 60,000, because 
      # there are 60 seconds to a minute 
      # and 1,000 milliseconds to a second.
      expired = new Date(now.getTime() + (ttl * 1))

      attributes = 
        name: req.body.name
        phone: req.body.phone
        price: req.body.price
        city: req.body.city
        category: req.body.category
        ttl: expired
        description: req.body.description

      ad = new ad_model attributes

      ad.save (err) -> 
        if err
          res.render "#{__dirname}/../views/ads/_form",
            title: "Iklan Gratis"
            stylesheet: 'sidewalk'
            error: err
        else
          if socketIO = app.settings.socketIO
            socketIO.sockets.emit "ad:new", ad

          res.redirect '/'

module.exports = routes