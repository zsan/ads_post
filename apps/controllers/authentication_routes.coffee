routes = (app,passport) ->
  app.get '/login', (req,res) ->
    return res.redirect "/" if req.isAuthenticated()
    res.render "#{__dirname}/../views/authentication/login",
      title: 'Login'
      stylesheet: 'login'
      message: req.session.messages

  app.post '/login', (req,res,next) ->
    (passport.authenticate 'local', (err,user,info) ->
      if err
        return next(err)

      if !user
        req.session.messages = info.message
        return res.redirect '/login'

      req.logIn user, (err) ->
        if err
          return next(err)
        return res.redirect '/admin/ads'
    )(req,res,next)    

  app.get '/logout', (req,res) ->
    req.logout()
    res.redirect '/'


module.exports = routes
