require 'mongoose-query-paginate'

routes = (app, ad_model) ->
  app.get '/', (req, res) ->
    options = 
      perPage: 5
      delta: 5
      page: 1

    query = ad_model.find().sort created_at: -1

    query.paginate options, (err, ads) ->
        res.render "#{__dirname}/../views/public/index",
        title: "Iklan Gratis"
        ads: ads

  app.namespace '/iklan', ->
    app.get '/', (req, res) ->
      page = 1
      options = 
        perPage: 10
        delta: 10
        page: page

      query = ad_model.find().sort created_at: -1

      query.paginate options, (err, ads) ->
        res.render "#{__dirname}/../views/public/ads_list",
          title: "Iklan Gratis"
          stylesheet: 'sidewalk'
          ads: ads,
          currentUrl: req.path

    app.get "/search", (req, res) ->
        startPage = req.query.iDisplayStart
        rangePage = req.query.iDisplayLength
        rangePage = 100 if rangePage > 100;
        page      = (startPage / rangePage) + 1          
        name      = req.query.sSearch
        
        options = 
          perPage: rangePage
          delta: rangePage
          page: page

        query = ad_model.find({name: new RegExp(name, 'i') }).sort created_at: -1


        query.paginate options, (err, ads) ->
          aaData= []
          ads["results"].forEach (dbRes) ->
            aaData.push([dbRes.created_at,dbRes.price,"<a href='/ads/#{dbRes.id}'>"+dbRes.name+"</a>"])
          
          res.json
            "iTotalRecords": ads.count,
            "iTotalDisplayRecords": ads.count,
            "aaData": aaData,
            "sEcho": req.query.sEcho

    ['mobil','komputer','lain_lain', 'alat_musik', 'buku','handphone','kuliner', 'motor', 'obat','pakaian','rumah'].forEach (b) ->
      app.get "/search", (req, res) ->

      app.namespace "/#{b}", ->
        app.get "/", (req, res) ->
          options = 
            perPage: 10
            delta: 10
            page: 1

          query = ad_model.find({category: b}).sort created_at: -1

          query.paginate options, (err, ads) ->
            aaData = ([x] for x in ads["results"])
            console.log aaData
            res.render "#{__dirname}/../views/public/ads_list",
              title: "Iklan Gratis"
              stylesheet: 'sidewalk'
              ads: ads
              currentUrl: req.path


        app.get "/search", (req, res) ->
          startPage = req.query.iDisplayStart
          rangePage = req.query.iDisplayLength
          rangePage = 100 if rangePage > 100;
          page      = (startPage / rangePage) + 1          
          name      = req.query.sSearch
          
          options = 
            perPage: rangePage
            delta: rangePage
            page: page

          query = ad_model.find({category: b, name: new RegExp(name, 'i') }).sort created_at: -1


          query.paginate options, (err, ads) ->
            aaData= []
            ads["results"].forEach (dbRes) ->
              aaData.push([dbRes.created_at,dbRes.price,"<a href='/ads/#{dbRes.id}'>"+dbRes.name+"</a>"])
            
            res.json
              "iTotalRecords": ads.count,
              "iTotalDisplayRecords": ads.count,
              "aaData": aaData,
              "sEcho": req.query.sEcho
          
module.exports = routes