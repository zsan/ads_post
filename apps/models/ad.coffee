#var user      = require('./schema/users')(mongoose);

ad = (mongoose) ->

  Schema = mongoose.Schema
  AdSchema = new Schema
    name: 
      type: String
      required: true
    phone:
      type: Number
      required: true
    price: 
      type: Number
      required: true
    city: 
      type: String
      required: true
    category: 
      type: String
      required: true
      enum: [
        'mobil',
        'komputer',
        'lain_lain', 
        'alat_musik', 
        'buku',
        'handphone',
        'kuliner', 
        'motor', 
        'obat',
        'pakaian',
        'rumah'
      ]
    description: 
      type: String
      required: true
    created_at:  
      type: Date
      default: Date.now 
    ttl:  
      type: Date
      index:
        expires: 3600
      default: Date.now
  mongoose.model 'Ad', AdSchema


module.exports = ad
