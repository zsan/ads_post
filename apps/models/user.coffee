# mongoose = require 'mongoose'
bcrypt   = require 'bcrypt'


user = (mongoose) ->
  # user schema
  Schema   = mongoose.Schema
  SALT_WORK_FACTOR = 10;

  UserSchema = new Schema
    username: 
      type: String
      required: true
      unique: true
    email: 
      type: String
      require: true
      unique: true
    password: 
      type: String
      required: true


  # encrypt password before save
  UserSchema.pre 'save', (next) ->
    user = this

    if !user.isModified 'password' 
      return next()

    bcrypt.genSalt SALT_WORK_FACTOR, (err,salt) ->
      return next err if err

      bcrypt.hash user.password, salt, (err,hash) ->
        return next err if err
        user.password = hash
        next()
        return

  # password verification
  UserSchema.methods.comparePassword = (candidatePassword, cb) ->
    bcrypt.compare candidatePassword, this.password, (err,isMatch) ->
      return cb err if err
      cb(null, isMatch)


  mongoose.model 'User', UserSchema

module.exports = user
