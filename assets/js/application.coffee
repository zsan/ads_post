#= require ../../public/components/jquery/jquery.min.js
#= require underscore-min.js

jQuery ->
  # Home page
  if window.location.pathname is '/iklan'
    refresh = ->
      window.location = '/iklan'

    socket = io.connect("/")
    socket.on "ad:new", (pie) ->
      refresh()
    socket.on "ad:remove", (pie) ->
      refresh()

    setTimeout refresh, 1000*60

    # DEBUG
    window.socket = socket

  # Admin
  $('ul#listing li div.status div').click ->
    dataId      = $(@).closest('li').attr('data-id')
    classNames  = $(@).attr('class').split(' ')
    statesArray = _.reject classNames, (className) -> className is 'on'
    state       = statesArray[0]
    $.ajax "/admin/pies/#{dataId}",
      type:'PUT'
      data: {state}
    $(@).closest('li').find('div.status div').removeClass 'on'
    $(@).addClass 'on'
