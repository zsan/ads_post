db = (mongoose) ->

  mongoUri = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/test'; 

  mongoose.connect(mongoUri);
  db = mongoose.connection;
  
  db.on 'error', console.error.bind console, 'connection error'
  
  db.once 'open', ->
    console.log '**CONNECTED TO MONGODB**'
  
module.exports = db