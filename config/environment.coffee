config = (app, express) ->

  if 'development' == app.get 'env'
    app.use express.errorHandler()
  

  if 'test' == app.get 'env'
    app.use express.errorHandler()
    app.set 'port', 3001

  if 'production' == app.get 'env'
    app.use express.errorHandler()

module.exports = config