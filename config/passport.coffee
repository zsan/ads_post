
authz = (passport, User) ->
  LocalStrategy = require('passport-local').Strategy

  # serialize sessions
  passport.serializeUser (user, done) ->
    done(null, user.id)

  passport.deserializeUser (id, done) ->
    User.findById id, (err, User) ->
      done err, User


  # Defining auth strategy
  passport.use new LocalStrategy (username, password, done) ->
    User.findOne username: username, (err, user) ->
      return done err if err
      
      if not user
        return done null, false, message: 'unknown user ' + 'username'

      user.comparePassword password, (err, isMatch) ->
        return done err if err

        if isMatch
          return done(null, user)
        else
          return done(null, false, message: 'Invalid Password')
      return
    return
  return


module.exports = authz
